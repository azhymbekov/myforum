﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyForum.ViewModels
{
    public class MessageViewModel
    {
        public string Author { get; set; }
        public string Content { get; set; }
        public string Date { get; set; }
    }
}
