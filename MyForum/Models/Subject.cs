﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyForum.Models
{
    public class Subject
    {
        public int Id { get; set; }
        [Display(Name = "Название темы")]
        public string SubjectName {get; set;}
       
        [Display(Name = "Содержимое")]
        public string Content { get; set; }

        public DateTime Date { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public List<Message> Messages { get; set; }
        public Subject()
        {
            Messages = new List<Message>();
        }

    }
}
