﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyForum.Models;
using MyForum.ViewModels;

namespace MyForum.Controllers
{
    public class MessageController : Controller
    {
        private UserManager<User> userManager;
        private ApplicationContext ApplicationContext;

        public MessageController(UserManager<User> userManager, ApplicationContext ApplicationContext)
        {
            this.userManager = userManager;
            this.ApplicationContext = ApplicationContext;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(int subjectId, string userName, string content)
        {
            User user = await userManager.FindByNameAsync(userName);
            Message message = new Message
            {
                SubjectId = subjectId,
                Date = DateTime.Now,
                User = user,
                Content = content,
                UserId = user.Id
            };
            await ApplicationContext.Messages.AddRangeAsync(message);
            await ApplicationContext.SaveChangesAsync();
            MessageViewModel messageViewModel = new MessageViewModel
            {
                Content = message.Content,
                Author = user.UserName,
                Date = message.Date.ToShortDateString()
            };

            return Json(messageViewModel);
        }

        
       
    }
}