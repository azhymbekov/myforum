﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyForum.Models;

namespace MyForum.Controllers
{
    public class SubjectController : Controller
    {

        private UserManager<User> userManager;
        private ApplicationContext ApplicationContext;

        public SubjectController(UserManager<User> userManager, ApplicationContext ApplicationContext)
        {
            this.userManager = userManager;
            this.ApplicationContext = ApplicationContext;
        }


        [Authorize]
        public async Task<IActionResult> Create()
        {
            User user = await userManager.FindByNameAsync(User.Identity.Name);
            ViewBag.UserId = user.Id;
            return View();

        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(Subject subject)
        {
            if (ModelState.IsValid)
            {
                subject.Date = DateTime.Now;
                ApplicationContext.Subjects.Add(subject);
                await ApplicationContext.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Subject", new { subjectId = subject.Id });

        }
       

        public IActionResult Index(int subjectId)
        {
            Subject subject = ApplicationContext.Subjects.Include(s => s.User).Include(s=>s.Messages).ThenInclude(m => m.User).FirstOrDefault(s => s.Id == subjectId);
            return View(subject);
        }

        //public IActionResult Subjects()
        //{
        //    List<Subject> subjects = ApplicationContext.Subjects.Include(s => s.User).ToList();

        //    return View();
        //}
        public async Task<IActionResult> Subjects()
        {
            var applicationDbContext = ApplicationContext.Subjects.Include(p => p.User).OrderByDescending(p => p.Date);
            return View(await applicationDbContext.ToListAsync());
        }

        //[HttpPost]
        public IActionResult SubjectSearch(string name)
        {
            var allSubjects = ApplicationContext.Subjects.Where(a => a.SubjectName.Contains(name)).ToList();
            return PartialView(allSubjects);
        }



    }
}